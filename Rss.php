<?php

$servername = "DESKTOP-QAGCUR4";
$connectionInfo = array("database" => "orbelite", "CharacterSet" => "UTF-8");

$conn = sqlsrv_connect($servername, $connectionInfo);

$consut01 = "select top 50 * from [Scraping].[Noticia] order by Actualizado desc;";
$result01 = sqlsrv_query($conn, $consut01);

header("Content-Type: text/xml;charset=iso-8859-1");
$base_url = "http://localhost/Rss_Noticias/";

echo "<?xml version='1.0' encoding='UTF-8' ?>" . PHP_EOL;
echo "<rss version='2.0'>" . PHP_EOL;
echo "<channel>" . PHP_EOL;
echo "<title>Noticias</title>" . PHP_EOL;
echo "<link>" . $base_url . "index.php</link>" . PHP_EOL;
echo "<description>Noticias del último minuto</description>" . PHP_EOL;
echo "<language>es-es</language>" . PHP_EOL;

while ($row1 = sqlsrv_fetch_array($result01)) {

    $publish_Date = $row1[6];
    $fec = date_format($publish_Date, 'Y-m-d H:i:s');

    echo "<item xmlns:dc='ns:1'>" . PHP_EOL;
    echo "<title>" . htmlspecialchars($row1[2]) . "</title>" . PHP_EOL;
    echo "<link>" . htmlspecialchars($row1[1]) . "/</link>" . PHP_EOL;
    echo "<guid>" . htmlspecialchars(md5($row1[0])) . "</guid>" . PHP_EOL;
    echo "<pubDate>" . $fec . "</pubDate>" . PHP_EOL;
    echo "<dc:creator>" . htmlspecialchars($row1[9]) . "</dc:creator>" . PHP_EOL;
    echo "<description><![CDATA[" . htmlspecialchars(substr($row1[3], 0, 300)) . "]]></description>" . PHP_EOL;
    echo "<category>" . htmlspecialchars($row1[10]) . "</category>" . PHP_EOL;
    echo "</item>" . PHP_EOL;
}

echo '</channel>' . PHP_EOL;
echo '</rss>' . PHP_EOL;
?>

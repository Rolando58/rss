<?php
$feed_url = "http://localhost/Noticias_01/rss.php";
$object = new DOMDocument();
$object->load($feed_url);
$content = $object->getElementsByTagName("item");
?>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Noticias</title>
        <!-- CSS only -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    </head>
    <body>
        
        <div class="container">
            <h1>Ultimas Noticias</h1>
            <div class="row ">
                <div class="col">
                    <?php
                    foreach ($content as $row) {
                        echo '<div class="card">';
//                        <img src="..." class="card-img-top" alt="...">
                        echo '<div class="card-body">';
                        echo '<h5 class="card-title">' . $row->getElementsByTagName("title")->item(0)->nodeValue . '</h5>';
                        echo '<p class="card-text">' . $row->getElementsByTagName("description")->item(0)->nodeValue . '</p>';
                        echo '<p align="right"><i>Fuente: ' . $row->getElementsByTagNameNS("ns:1", "*")->item(0)->nodeValue . '</i></p>';
                        echo '<a href="' . $row->getElementsByTagName("link")->item(0)->nodeValue . '" class="btn btn-warning" target="_blank">Ir a la Información</a>';
                        echo '</div>';
                        echo '<div class="card-footer">';
                        echo '<small class="text-muted">' . $row->getElementsByTagName("category")->item(0)->nodeValue . '</small>';
                        echo '<p>' . $row->getElementsByTagName("pubDate")->item(0)->nodeValue . '</p><br>';

                        echo '</div>';
                        echo '</div><br>';
                    }
                    ?>
                </div>
            </div>
        </div>
    </body>
    <!-- JavaScript Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>
</html>
